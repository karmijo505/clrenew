<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Motorclub extends Model
{
    protected $primaryKey = 'naic_cocode';

    public function lobs() {

        return $this->hasOne('App\Models\Lob', 'lob_id', 'lob_id');
    }
}
