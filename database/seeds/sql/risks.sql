INSERT INTO risks VALUES ('Ophthalmic Mutual Insurance Company (A Risk Retention Group)',44105,'655 Beach Street','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Paratransit Insurance Company, A Mutual Risk Retention Group',44130,'2418 Airport Road Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('Western Pacific Mutual Insurance Company, A Risk Retention Group',40940,'9265 Madras Court','Littleton','CO','80130',13);
INSERT INTO risks VALUES ('Attorneys'' Liability Assurance Society, Inc. A Risk Retention Group',10639,'311 South Wacker Drive, Suite 5700','Chicago','IL','60606',13);
INSERT INTO risks VALUES ('Consumer Specialties Insurance Company Risk Retention Group',10075,'2418 Airport Road Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('Claim Professionals Liability Insurance Company (A Risk Retention Group)',12172,'2418 Airport Road Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('ARISE Boiler Inspection and Insurance Company Risk Retention Group',13580,'P O Box 23790','Louisville','KY','40223',13);
INSERT INTO risks VALUES ('Spirit Commercial Auto Risk Retention Group, Inc.',14207,'1605 Main Street, Suite 800','Sarasota','FL','34236',13);
INSERT INTO risks VALUES ('NASW Risk Retention Group, Inc.',14366,'8000 East Maplewood Avenue Suite 350','Greenwood Village','CO','80111',13);
INSERT INTO risks VALUES ('Bay Insurance Risk Retention Group, Inc.',15582,'146 Fairchild Street Suite 135','Charleston','SC','29492',13);
INSERT INTO risks VALUES ('Eagle Builders Insurance Company Risk Retention Group, Inc.',16104,'1605 Main Street Suite 800','Saratosa','FL','34236',13);
INSERT INTO risks VALUES ('CPA Mutual Insurance Company of America Risk Retention Group',10164,'126 College Street Suite 400','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Mental Health Risk Retention Group, Inc.',44237,'103 Eisenhower Parkway Suite 101','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('American Association of Orthodontists Insurance Company (A Risk Retention Group)',10232,'2418 Airport Road Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('Alliance of Nonprofits for Insurance, Risk Retention Group, Inc.',10023,'2418 Airport Road, Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('National Service Contract Insurance Company Risk Retention Group, Inc.',10234,'58 East View Lane Suite 2','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('Spirit Mountain Insurance Company Risk Retention Group, Inc.',10754,'1605 Main Street Suite 800','Saratosa','FL','34236',13);
INSERT INTO risks VALUES ('Oceanus Insurance Company, A Risk Retention Group',12189,'1327 Ashley River Road, Building C, Suite 200','Charleston','SC','29407',13);
INSERT INTO risks VALUES ('Green Hills Insurance Company, a Risk Retention Group',11941,'P O Box 530','Burlington','VT','	05402',13);
INSERT INTO risks VALUES ('Affiliates Insurance Reciprocal, a Risk Retention Group',13677,'30 Main Street, Suite 330','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Health Care Industry Liability Reciprocal Insurance Company, a risk retention gr',11832,'201 South Main Street, Suite 200','Ann Arbor','MI','48104',13);
INSERT INTO risks VALUES ('Doctors Company Risk Retention Group, a Reciprocal Exchange The',14347,'185 Greenwood Road','Napa','CA','94558-9270',13);
INSERT INTO risks VALUES ('Velocity Insurance Company, A Risk Retention Group',15956,'1327-C Ashley River Road, Suite 200','Charleston','SC','29407',13);
INSERT INTO risks VALUES ('Fairway Physicians Insurance Company, a Risk Retention Group',11840,'29899 Agoura Road, Suite 110','Agoura Hills','CA','91301',13);
INSERT INTO risks VALUES ('American Safety Risk Retention Group, Inc.',25448,'148 College Street Suite 204','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('National Home Insurance Company (A Risk Retention Group)',44016,'3025 South Parker Road, Suite 711','Aurora','CO','80014',13);
INSERT INTO risks VALUES ('States Self-Insurers Risk Retention Group, Inc.',44075,'222 South Ninth Street, Suite 2700','Minneapolis','MN','55402-3332',13);
INSERT INTO risks VALUES ('St. Charles Insurance Company Risk Retention Group',11114,'2700 North Third Street Suite 3050','Phoenix','AZ','85004',13);
INSERT INTO risks VALUES ('Preferred Physicians Medical Risk Retention Group, Inc.',44083,'11880 College Boulevard Sutie 300','Overland Park','KS','66210-2141',13);
INSERT INTO risks VALUES ('College Risk Retention Group, Inc.',13613,'P O Box 530','Burlington','VT','05402-0530',13);
INSERT INTO risks VALUES ('CrossFit Risk Retention Group, Inc.',13720,'2897 Kalawao Street','Honolulu','HI','96822',13);
INSERT INTO risks VALUES ('Terrafirma Risk Retention Group, LLC',14395,'100 Bank Street, Suite 610','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Doctors & Surgeons National Risk Retention Group, Inc.',13018,'3370 Sugarloaf Parkway, Suite G-2/302','Lawrenceville','GA','30044',13);
INSERT INTO risks VALUES ('Terra Insurance Company (A Risk Retention Group)',10113,'2386 Airport Road Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('Architects & Engineers Insurance Company A Risk Retention Group',44148,'2056 Westings Avenue, Suite 20','Naperville','IL','60563',13);
INSERT INTO risks VALUES ('OOIDA Risk Retention Group, Inc.',10353,'58 East View Lane, Suite 2','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('STICO Mutual Insurance Company, Risk Retention Group',10476,'76 St. Paul Street, Suite 500','Burlington','VT','05401-4471',13);
INSERT INTO risks VALUES ('Continuing Care Risk Retention Group, Inc.',11798,'58 East View Lane, Suite 2','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('Security America Risk Retention Group, Inc.',11267,'100 Bank Street, Suite 610','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('American Trucking and Transportation Insurance Company, A Risk Retention Group',11534,'PO Box 8509','Missoula','MT','59802',13);
INSERT INTO risks VALUES ('Applied Medico-Legal Solutions Risk Retention Group, Inc.',11598,'2555 East Camelback Road Suite 700','Phoenix','AZ','85016',13);
INSERT INTO risks VALUES ('Preferred Contractors Insurance Company Risk Retention Group, LLC',12497,'1620 Providence Road','Towson','MD','21286',13);
INSERT INTO risks VALUES ('COPIC, a Risk Retention Group',14906,'2386 Airport Road Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('OMS National Insurance Company, Risk Retention Group',44121,'6133 N. River Road Suite 650','Rosemont','IL','60018',13);
INSERT INTO risks VALUES ('National Catholic Risk Retention Group, Inc., The',10083,'148 College Street Suite 204','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Housing Authority Risk Retention Group, Inc.',26797,'189 Commerce Court','Cheshire','CT','06410-0189',13);
INSERT INTO risks VALUES ('American Excess Insurance Exchange, Risk Retention Group',10903,'150 Dorset Street, PMB # 238','South Burlington','VT','	05403',13);
INSERT INTO risks VALUES ('ProBuilders Specialty Insurance Company, RRG',11671,'3025 S. Parker Road, Suite 711','Aurora','CO','80014',13);
INSERT INTO risks VALUES ('Titan Insurance Company, Inc., A Risk Retention Group',11153,'5215 North O''Connor Blvd., Suite 1200','Irving','TX','75039',13);
INSERT INTO risks VALUES ('Allied Professionals Insurance Company, A Risk Retention Group, Inc.',11710,'1100 West Town & Country Road Suite 1400','Orange','CA','92868',13);
INSERT INTO risks VALUES ('Restoration Risk Retention Group, Inc.',12209,'76 St. Paul Street, Suite 500','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Contractors Insurance Company of North America, Inc., a Risk Retention Group',11603,'201 Merchant Street, Suite 2400','Honolulu','HI','96813',13);
INSERT INTO risks VALUES ('Association of Certified Mortgage Originators Risk Retention Group, Inc.',14425,'1605 Main Street Suite 800','Saratosa','FL','34236',13);
INSERT INTO risks VALUES ('Hospitality Risk Retention Group, Inc.',16038,'c/o Risk Services','Saratosa','FL','34236',13);
INSERT INTO risks VALUES ('American Builders Insurance Company Risk Retention Group, Inc.',12631,'5151 Hampstead High Street, Suite 200','Montgomery','AL','36116',13);
INSERT INTO risks VALUES ('American Contractors Insurance Company Risk Retention Group',12300,'2600 N Central Expressway, Suite 800','Richardson','TX','75080',13);
INSERT INTO risks VALUES ('United Educators Insurance, a Reciprocal Risk Retention Group',10020,'7700 Wisconsin Avenue, Suite 500','Bethesda','MD','20814',13);
INSERT INTO risks VALUES ('Bonded Builders Insurance Company, A Risk Retention Group',13010,'c/o Risk Services','Washington','DC','20007',13);
INSERT INTO risks VALUES ('AttPro RRG Reciprocal Risk Retention Group',13795,'5814 Reed Road','Fort Wayne','IN','46835',13);
INSERT INTO risks VALUES ('MMIC Risk Retention Group, Inc.',14062,'7701 France Avenue South, Suite 500','Minneapolis','MN','55435-5288',13);
INSERT INTO risks VALUES ('Coverys RRG, Inc.',14160,'One Financial Center, 13th Floor','Boston','MA','	02111',13);
INSERT INTO risks VALUES ('County Hall Insurance Company, Inc., A Risk Retention Group',15947,'401 Hawthorn Lane Suite 110 # 226','Charlotte','NC','28204',13);
INSERT INTO risks VALUES ('DAN Risk Retention Group, Inc.',15928,'1327C Ashley River Road, Suite 200','Charleston','SC','29407',13);
INSERT INTO risks VALUES ('Title Industry Assurance Company Risk Retention Group',10084,'76 St. Paul Street, Suite 500','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Golden Insurance Company, A Risk Retention Group',11145,'18835 North Thompson Peak Parkway, Suite 210','Scottsdale','AZ','85255',13);
INSERT INTO risks VALUES ('Marathon Financial Insurance Company, Inc., A Risk Retention Group',11117,'PO Box 961','O''Fallon','IL','62269',13);
INSERT INTO risks VALUES ('National Independent Truckers Insurance Company, A Risk Retention Group',11197,'1327 Ashley River Road, Building C, Suite 200','Charleston','SC','29407',13);
INSERT INTO risks VALUES ('Yellowstone Insurance Exchange (A Risk Retention Group)',11796,'2418 Airport Road Suite 2A','Barre','VT','	05641',13);
INSERT INTO risks VALUES ('Caring Communities, A Reciprocal Risk Retention Group',12373,'1850 W. Winchester, Suite 109','Libertyville','IL','60048',13);
INSERT INTO risks VALUES ('ARCOA Risk Retention Group, Inc.',13177,'2555 East Camelback Road Suite 700','Phoenix','AZ','85016',13);
INSERT INTO risks VALUES ('Cherokee Guarantee Company, Inc. A Risk Retention Group',14388,'18835 N. Thompson Peak Pkwy, Suite 210','Scottsdale','AZ','85255',13);
INSERT INTO risks VALUES ('Academic Medical Professionals Insurance Risk Retention Group, LLC',12934,'76 St. Paul Street Suite 500','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Romulus Insurance Risk Retention Group, Inc.',15744,'1327-C Ashley River Road, Suite 200','Charleston','SC','29407',13);
INSERT INTO risks VALUES ('Mutual Risk Retention Group, Inc. The',26257,'3000 Oak Road Suite 600','Walnut Creek','CA','94597',13);
INSERT INTO risks VALUES ('New Home Warranty Insurance Company, A Risk Retention Group',13792,'2233 Wisconsin Avenue, NW, Suite 310','Washington','DC','20007',13);
INSERT INTO risks VALUES ('Mountain States Healthcare Reciprocal Risk Retention Group',11585,'100 Bank Street, Suite 500','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('Lone Star Alliance, Inc., a Risk Retention Group',15211,'40 Main Street Suite 500','Burlington','VT','	05401',13);
INSERT INTO risks VALUES ('California Medical Group Insurance Company, Risk Retention Group',12180,'1001 Bishop Street','Honolulu','HI','96813',13);
INSERT INTO risks VALUES ('ICI Mutual Insurance Company, A Risk Retention Group',11268,'1401 H Street, North West Suite 1000','Washington','DC','20005',13);
INSERT INTO risks VALUES ('MedChoice Risk Retention Group, Inc. ',15738,'P.O. Box 713','Milton','VT','	05468',13);
